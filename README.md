# Transparent_MEA

Here, we describe an open-source transparent MEA based on the OpenEphys platform. This resource is designed to be accessible, requiring minimal microelectrode fabrication or circuit design experience. 